import axios from "axios";
const baseUrl = process.env.REACT_APP_API_URL;
var defaultHeaders = {
  Accept: "application/json",
  "Content-Type": "application/json",
  "Access-Control-Allow-Origin": "*"
};

const Api = async function(endpoint, method, params, additionalHeaders) {
  const headers = {
    ...defaultHeaders,
    ...additionalHeaders
  };
  var fullUrl = baseUrl + endpoint;
  var getToken = await localStorage.getItem("auth_token");
  var getSessionId = await localStorage.getItem("session_id");
  if (getToken && getSessionId) {
    headers.Authorization = getToken;
    headers.session_id = getSessionId;
  }

  if (method == "get" || method == "GET") {
    const resp = await axios
      .get(fullUrl, {
        params: params,
        headers: headers
      })
      .then(function(response) {
        return { response: response.data, status: response.status };
      })
      .catch(async function(error) {
        if (error.response) {
          if (parseInt(error.response.status) == 401) {
            redirectToLogin();
          }
          return {
            response: error.response.data,
            status: error.response.status
          };
        }
      });

    return resp;
  } else if (method == "post") {
    const resp = await axios
      .post(fullUrl, params, {
        headers: headers
      })
      .then(function(response) {
        return { response: response.data, status: response.status };
      })
      .catch(async function(error) {
        if (error.response) {
          if (parseInt(error.response.status) == 401) {
            redirectToLogin();
          }
          return {
            response: error.response.data,
            status: error.response.status
          };
        }
      });

    return resp;
  } else if (method == "put") {
    const resp = await axios
      .put(fullUrl, params, {
        headers: headers
      })
      .then(function(response) {
        return { response: response.data, status: response.status };
      })
      .catch(async function(error) {
        if (error.response) {
          if (parseInt(error.response.status) == 401) {
            redirectToLogin();
          }
          return {
            response: error.response.data,
            status: error.response.status
          };
        }
      });

    return resp;
  } else if (method == "delete") {
    const resp = await axios
      .delete(fullUrl, {
        headers: headers,
        data: params
      })
      .then(function(response) {
        return { response: response.data, status: response.status };
      })
      .catch(async function(error) {
        if (error.response) {
          if (parseInt(error.response.status) == 401) {
            redirectToLogin();
          }
          return {
            response: error.response.data,
            status: error.response.status
          };
        }
      });

    return resp;
  }
};

const redirectToLogin = () => {
  window.location = "/auth/login";
};

export { Api, baseUrl };
