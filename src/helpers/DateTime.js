import moment from "moment";

const UtcTimeStamp = () => {
  return moment().utc().format('X');
};

export {
    UtcTimeStamp
}