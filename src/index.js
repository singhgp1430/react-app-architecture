import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import AuthLayout from "layouts/Auth.js";
import AdminLayout from "layouts/Admin.js";
import PrivateRoute from "layouts/PrivateRoute.js";

const hist = createBrowserHistory();

function App() {
  
    return (
          <Router history={hist}>
            <Switch>
              <Route path="/auth" component={AuthLayout} />
              <PrivateRoute RenderComponent={AdminLayout} path="/admin" />
              <Redirect from="/" to="/auth" />
            </Switch>
          </Router>
    );
}

ReactDOM.render(<App />, document.getElementById("root"));
