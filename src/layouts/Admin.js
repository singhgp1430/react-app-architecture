import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import routes from "routes";

class AdminLayout extends React.Component{
    
    getRoutes = (routes) => {
        return routes.map((prop, key) => {
          if (prop.collapse) {
            return this.getRoutes(prop.views);
          }
          if (prop.layout === "/admin") {
            return (
              <Route
                path={prop.layout + prop.path}
                component={prop.component}
                key={key}
              />
            );
          } else {
            return null;
          }
        });
    };

    render(){
        return(
          <Switch>
            {this.getRoutes(routes)}
            <Redirect from="/auth" to="/admin/dashboard" />
          </Switch>
        )
    }
}

export default AdminLayout;