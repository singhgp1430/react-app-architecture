import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import routes from "routes";

class AuthLayout extends React.Component{
    

        getRoutes = (routes) => {
        return routes.map((prop, key) => {
          if (prop.collapse) {
            return this.getRoutes(prop.views);
          }
          if (prop.layout === "/auth") {
            return (
              <Route
                path={prop.layout + prop.path}
                component={prop.component}
                key={key}
              />
            );
          } else {
            return null;
          }
        });
      };

    render(){
        return(
         <Switch>
            {this.getRoutes(routes)}
            <Redirect from="/auth" to="/auth/login" />
          </Switch>
        )
    }
}

export default AuthLayout;