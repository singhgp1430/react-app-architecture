import React from "react";
import PropTypes from "prop-types";
import { Route } from "react-router-dom";

class PrivateRoute extends React.Component {
  render() {
    //const { isLoggedIn } = this.props.loginData;
    const { RenderComponent } = this.props;
    return (
      <Route
        {...this.props}
        render={props =>
        //   isLoggedIn ? 
          <RenderComponent {...props} /> 
        //   : <Redirect to="/auth" />
        }
      />
    );
  }
}

PrivateRoute.propTypes = {
  loginData: PropTypes.object,
  children: PropTypes.node,
  RenderComponent: PropTypes.any
};

// const mapStateToProps = state => {
//   return {
//     loginData: state.userData
//   };
// };

export default PrivateRoute;
