import {
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE,
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAILURE,
  FETCHING_USER_SUCCESS,
  LOGOUT,
  OTHER_LOGIN,
  GET_USERS,
  GET_USERS_SUCCESS,
  GET_USERS_FAILURE
} from "../constants/user";
import { apiLogin, apiGetUsers } from "../api/user";

export const doLogin = () => {
  return { type: LOGIN_USER };
};

export const doStoreLogout = () => {
  return { type: LOGOUT };
};

export const loginSuccess = data => {
  return { type: LOGIN_USER_SUCCESS, data };
};

export const otherlogin = data => {
  return { type: OTHER_LOGIN, data };
};

export const loginFailure = data => {
  return { type: LOGIN_USER_FAILURE, data };
};

export const doSignup = () => {
  return { type: SIGNUP_USER };
};

export const fetchUserProfile = data => {
  return { type: FETCHING_USER_SUCCESS, data };
};

export const signupSuccess = data => {
  return { type: SIGNUP_USER_SUCCESS, data };
};

export const signupFailure = data => {
  return { type: SIGNUP_USER_FAILURE, data };
};

export const getUsers = data => {
  return { type: GET_USERS, data };
};

export const getUsersSuccess = data => {
  return { type: GET_USERS_SUCCESS, data };
};

export const getUsersFailure = data => {
  return { type: GET_USERS_FAILURE, data };
};

export const authLogin = params => {
  return dispatch => {
    dispatch(doLogin());
    apiLogin(params)
      .then(([response, status]) => {
        if (status) {
          return dispatch(loginSuccess(response));
        } else {
          return dispatch(loginFailure(response));
        }
      })
      .catch(error => console.log(error, "error"));
  };
};

export const fetchUser = () => {
  return dispatch => {
    apiGetUsers()
      .then(([response, status]) => {
        if (status) {
          dispatch(fetchUserProfile(response));
        }
        return true;
      })
      .catch(error => console.log(error));
  };
};

// get all users
export const getAdminUsers = () => {
  return dispatch => {
    dispatch(getUsers());
    apiGetUsers()
      .then(([response, status]) => {
        if (status) {
          dispatch(getUsersSuccess(response));
        } else {
          dispatch(getUsersFailure(response));
        }
        return true;
      })
      .catch(error => console.log(error));
  };
};

export const doLogout = () => {
  return dispatch => {
    dispatch(doStoreLogout());
  };
};
