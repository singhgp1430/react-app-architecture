import { Api } from "helpers/Api";
/*eslint-disable eqeqeq*/
export const apiLogin = async params => {
  const resp = await Api("login", "post", params);
  if (parseInt(resp.response.code) == 200) {
    localStorage.setItem("auth_token", resp.response.data.auth_token);
    localStorage.setItem("session_id", resp.response.data.session_id);
    return Promise.all([resp.response, true]);
  } else {
    return Promise.all([resp.response, false]);
  }
};

export const apiGetUsers = async () => {
  const resp = await Api("users", "get", {});
  if (parseInt(resp.response.code) == 200) {
    return Promise.all([resp.response, true]);
  } else {
    return Promise.all([resp.response, false]);
  }
};

export const apiGetPermissions = async () => {
  const resp = await Api("permissions", "get", {});
  if (parseInt(resp.response.code) == 200) {
    return Promise.all([resp.response, true]);
  } else {
    return Promise.all([resp.response, false]);
  }
};
