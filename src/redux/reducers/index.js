import { combineReducers } from "redux";
import permissionReducer from "./permissionsReducer";
import userReducer from "./userReducer";
import { reducer as formReducer } from "redux-form";
export default combineReducers({
  permissionData: permissionReducer,
  userData: userReducer,
  form: formReducer
});
