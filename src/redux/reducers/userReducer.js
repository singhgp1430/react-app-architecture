import {
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE,
  GET_USERS_SUCCESS,
  GET_USERS_FAILURE,
  GET_USERS,
  LOGOUT
} from "../constants/user";

const initialState = {
  data: [],
  isLoggedIn: false,
  loggingIn: false,
  error: false,
  isSignedup: false,
  signUp: false,
  otherloggingIn: false,
  adminData: [],
  fetchingAdminUsers: false,
  isFetchedUsers: false,
  isFetchError: false
};

let userReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_USER:
      return {
        ...state,
        data: [],
        isLoggedIn: false,
        loggingIn: true,
        otherloggingIn: false
      };
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        data: action.data,
        isLoggedIn: true,
        loggingIn: false,
        otherloggingIn: false,
        error: false
      };
    case LOGIN_USER_FAILURE:
      return {
        ...state,
        isLoggedIn: false,
        loggingIn: false,
        error: true,
        data: action.data,
        otherloggingIn: false
      };
    case GET_USERS:
      return {
        ...state,
        adminData: [],
        fetchingAdminUsers: true,
        isFetchedUsers: false,
        isFetchError: false
      };
    case GET_USERS_SUCCESS:
      return {
        ...state,
        adminData: action.data,
        fetchingAdminUsers: false,
        isFetchedUsers: true,
        isFetchError: false
      };
    case GET_USERS_FAILURE:
      return {
        ...state,
        adminData: action.data,
        fetchingAdminUsers: false,
        isFetchedUsers: false,
        isFetchError: true
      };
    case LOGOUT:
      return {
        ...state,
        data: [],
        isLoggedIn: false,
        loggingIn: false,
        error: false,
        isSignedup: false,
        signUp: false,
        otherloggingIn: false
      };
    default:
      return state;
  }
};

export default userReducer;
