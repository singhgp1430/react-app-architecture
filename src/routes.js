import Dashboard from "views/Dashboard/Dashboard.js";
import Login from "views/Pages/Login.js";


const routes = [
    {
      path: "/login",
      name: "Login",
      icon: "",
      component: Login,
      sidebar: true,
      layout: "/auth"
    },
    {
      path: "/dashboard",
      name: "Dashboard",
      icon: "",
      component: Dashboard,
      sidebar: true,
      layout: "/admin"
    }
]

export default routes