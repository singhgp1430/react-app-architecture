import React from "react";
import {Field, reduxForm} from "redux-form";
import TextField from "components/CustomInputs/TextField"
function LoginForm(){
    return(
        <div>
        <Field
            name="username"
            type="text"
            component={TextField}
            label={"Enter Username"}
        />
        <Field
            name="password"
            type="password"
            component={TextField}
            label={"Enter Password"}
        />
        </div>
    )
}

LoginForm = reduxForm({
    // a unique name for the form
    form: "login_form"
})(LoginForm);

export default LoginForm;